//
//  User.swift
//  Attendance
//
//  Copyright © 2019 Giovanni Giacomo. All rights reserved.
//

import Combine
import SwiftUI
import SwiftWebSocket

let serverAddress = "ws://18.231.179.3:8080"

enum ActiveAlert {
    case failed, success
}

class User: ObservableObject {
    let objectWillChange = PassthroughSubject<User, Never>()
    
    var ws: WebSocket
    var session: String = ""
    
    var attendance: Float = 0.0
    var userId: String = ""
    var userType: String = ""
    
    // Add Course Variables
    var showAddCourseAlert: Bool = false {
        didSet {
            objectWillChange.send(self)
        }
    }
    var activeAddCourseAlert: ActiveAlert = .failed
    var addCourseMessage: String = ""
    
    // Attendance Variables
    var showTokenAlert: Bool = false {
        didSet {
            objectWillChange.send(self)
        }
    }
    var activeTokenAlert: ActiveAlert = .failed
    
    // Classes Variables
    var allClasses: [Class] = [] {
        didSet {
            objectWillChange.send(self)
        }
    }
    var myClasses: [Class] = [] {
        didSet {
            objectWillChange.send(self)
        }
    }
    
    // Course Variables
    var allCourses: [Course] = [] {
        didSet {
            objectWillChange.send(self)
        }
    }
    var myCourses: [Course] = [] {
        didSet {
            objectWillChange.send(self)
        }
    }
    
    // Login Variables
    var hasFailedToLogin: Bool = false {
        didSet {
            objectWillChange.send(self)
        }
    }
    
    var isLoggedIn: Bool = false {
        didSet {
            objectWillChange.send(self)
        }
    }
    
    // Join Course Variables
    var joinedCourse: String = ""
    
    // Register Variables
    var showRegisterAlert: Bool = false {
        didSet {
            objectWillChange.send(self)
        }
    }
    var activeRegisterAlert: ActiveAlert = .failed
    var registerMessage: String = ""

    // Rollcall Variables
    var hasAddedRollcall: Bool = false {
        didSet {
            objectWillChange.send(self)
        }
    }
    
    // Student Variables
    var allStudents: [Student] = [] {
        didSet {
            objectWillChange.send(self)
        }
    }
    var myStudents: [Student] = [] {
        didSet {
            objectWillChange.send(self)
        }
    }
    
    init() {
        ws = WebSocket(serverAddress)
        setupWebSockets()
    }
    
    deinit {
        if ws.readyState == .connecting || ws.readyState == .open {
            ws.close()
        }
    }
    
    func setupWebSockets() {
        // Setup the event when client connects to server.
        ws.event.open = {
            print("Connection to server (\(serverAddress)) established...")
        }
        
        // Setup the event when client disconnects from server.
        ws.event.close = { code, reason, clean in
            print("Connection to server (\(serverAddress)) terminated...")
        }
        
        // Setup the event for when the client receives a message from the server.
        ws.event.message = { msg in
            let str = String(describing: msg)
            let data: Message = loadJSON(str)
            
            switch data.header {
            case "login-answer":
                self.handleLogin(data: loadJSON(str))
            case "register-answer":
                self.handleRegister(data: loadJSON(str))
            case "list-courses-answer":
                self.handleListCourses(data: loadJSON(str))
            case "add-course-answer":
                self.handleAddCourse(data: loadJSON(str))
            case "add-student-to-course-answer":
                self.handleAddStudentToCourse(data: loadJSON(str))
            case "list-classes-answer":
                self.handleListClasses(data: loadJSON(str))
            case "rollcall-answer":
                self.handleRollcall(data: loadJSON(str))
            case "attendance-answer":
                self.handleAttendance(data: loadJSON(str))
            case "list-students-answer":
                self.handleListStudents(data: loadJSON(str))
            default:
                return
            }
            
            print("Received message from server (\(serverAddress)): \(str)")
        }
    }
    
    func handleLogin(data: LoginAnswerMessage) {
        if data.authenticated == true {
            self.isLoggedIn = true
            self.userId = data.id
            self.userType = data.type
        } else {
            self.hasFailedToLogin = true
        }
    }
    
    func handleRegister(data: RegisterAnswerMessage) {
        if data.authenticated {
            self.activeRegisterAlert = .success
            self.registerMessage = "O registro foi bem sucedido."
        } else {
            self.activeRegisterAlert = .failed
            self.registerMessage = data.error
        }
        
        self.showRegisterAlert = true
    }
    
    func handleListCourses(data: ListCoursesAnswerMessage) {
        if (data.courses) != nil {
            if data.type == "all" {
                self.allCourses = data.courses!
            } else {
                self.myCourses = data.courses!
            }
        }
    }
    
    func handleAddCourse(data: AddCourseAnswerMessage) {
        if data.authenticated {
            self.activeAddCourseAlert = .success
            self.addCourseMessage = "A nova disciplina foi adicionada com sucesso."
            
            let msg = saveJSON(ListCoursesByUserMessage(registration: self.userId, type: self.userType))!
            
            // Attempt to send message to server if connection has been established.
            if self.ws.readyState == .open {
                self.ws.send(msg)
                print("Sent message: \(msg)")
            } else {
                print("Failed to send message: \(msg)")
            }
            
        } else {
            self.activeAddCourseAlert = .failed
            self.addCourseMessage = data.error
        }
        
        self.showAddCourseAlert = true
    }
    
    func handleAddStudentToCourse(data: AddStudentToCourseAnswerMessage) {
        if data.success == true {
            let msg1 = saveJSON(ListCoursesByUserMessage(registration: self.userId, type: self.userType))!
            let msg2 = saveJSON(ListClassesByCourseMessage(course: self.joinedCourse))!
            
            // Attempt to send message to server if connection has been established.
            if self.ws.readyState == .open {
                self.ws.send(msg1)
                self.ws.send(msg2)
                print("Sent message: \(msg1)")
                print("Sent message: \(msg2)")
            } else {
                print("Failed to send message: \(msg1)")
                print("Failed to send message: \(msg2)")
            }
            
            if self.userType == "Aluno" {
                let msg3 = saveJSON(ListClassesByUserMessage(course: self.joinedCourse, student: self.userId))!
                
                // Attempt to send message to server if connection has been established.
                if self.ws.readyState == .open {
                    self.ws.send(msg3)
                    print("Sent message: \(msg3)")
                } else {
                    print("Failed to send message: \(msg3)")
                }
            }
        }
    }
    
    func handleListClasses(data: ListClassesAnswerMessage) {
        if (data.classes) != nil {
            if data.type == "all" {
                self.allClasses = data.classes!
            } else {
                self.myClasses = data.classes!
            }
        } else {
            if data.type == "all" {
                self.allClasses = []
            } else {
                self.myClasses = []
            }
        }
        
        if self.allClasses.count != 0 {
            self.attendance = 100.0 * Float(self.myClasses.count) / Float(self.allClasses.count)
        }
    }
    
    func handleRollcall(data: RollcallAnswerMessage) {
        if data.authenticated {
            self.hasAddedRollcall = true
        }
    }
    
    func handleAttendance(data: AttendanceAnswerMessage) {
        if data.authenticated {
            self.activeTokenAlert = .success
        } else {
            self.activeTokenAlert = .failed
        }
        
        self.showTokenAlert = true
    }
    
    func handleListStudents(data: ListStudentsAnswerMessage) {
        if (data.students) != nil {
            if data.type == "all" {
                self.allStudents = data.students!
            } else {
                self.myStudents = data.students!
            }
        } else {
            if data.type == "all" {
                self.allStudents = []
            } else {
                self.myStudents = []
            }
        }
    }
}
