//
//  Preview.playground
//  Attendance
//
//  Copyright © 2019 Giovanni Giacomo. All rights reserved.
//

import SwiftUI
import PlaygroundSupport

struct Course: Codable, Hashable, Identifiable {
    var id: Int
    var name: String
    var description: String
}

struct CourseItem: View {
    var course: Course
    
    var body: some View {
        VStack(alignment: .leading, spacing: 16.0) {
            //
            Image("ImageExample")
                .resizable()
                .renderingMode(.original)
                .aspectRatio(contentMode: .fill)
                .frame(width: 300, height: 170)
                .cornerRadius(10)
                .shadow(radius: 10)
            
            VStack(alignment: .leading, spacing: 5.0) {
                //
                Text(course.name)
                    .font(.headline)
                
                //
                Text(course.description)
                    .font(.subheadline)
                    .multilineTextAlignment(.leading)
                    .lineLimit(2)
                        .frame(height: 40)
            }
        }
    }
}

let courseExample = Course(id: 1, name: "Design Patterns", description: "Lorem ipsum blablablablabla...")

// Present the view controller in the Live View window.
PlaygroundPage.current.liveView = UIHostingController(rootView: CourseItem(course: courseExample))
