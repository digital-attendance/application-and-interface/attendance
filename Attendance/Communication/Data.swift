//
//  Data.swift
//  Attendance
//
//  Copyright © 2019 Giovanni Giacomo. All rights reserved.
//

import Foundation

let courseData: [Course] = [
    Course(id: "1", name: "Padrões de Projeto", description: "Solução geral para um problema que ocorre com frequência dentro de um determinado contexto no projeto de software."),
    Course(id: "2", name: "Computação Gráfica", description: "A computação gráfica é a área da computação destinada à geração de imagens em geral — em forma de representação de dados e informação, ou em forma de recriação do mundo real. Ela pode possuir uma infinidade de aplicações para diversas áreas, desde a própria informática, ao produzir interfaces gráficas para software, sistemas operacionais e sites na Internet, quanto para produzir animações e jogos."),
    Course(id: "3", name: "Ciências Ambientais", description: "Ciências do ambiente são um campo académico multidisciplinar que integra ciências físicas, biológicas e da informação para o estudo do ambiente e soluções para problemas ambientais. Entre estas ciências estão a ecologia, biologia, física, química, zoologia, mineralogia, oceanografia, limnologia, ciências do solo, geologia, ciências da atmosfera, geografia e geodesia."),
    Course(id: "4", name: "Economia", description: "Economia (ciência económica (pt) ou ciência econômica (pt-BR)) é uma ciência que consiste na análise da produção, distribuição e consumo de bens e serviços. É também a ciência social que estuda a atividade económica, através da aplicação da teoria económica, tendo, na gestão, a sua aplicabilidade prática.")
]

func loadJSON<T: Decodable>(_ message: String, as type: T.Type = T.self) -> T {
    let data: Data = message.data(using: .utf8)!

    do {
        return try JSONDecoder().decode(T.self, from: data)
    } catch {
        fatalError("Could not parse \(message) as \(T.self):\n\(error)")
    }
}

func saveJSON<T: Encodable>(_ data: T) -> String? {
    do {
        let json = try JSONEncoder().encode(data)
        return String(data: json, encoding: .utf8)
    } catch {
        fatalError("Could not encode \(T.self) in JSON:\n\(error)")
    }
}
