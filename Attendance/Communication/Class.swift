//
//  Class.swift
//  Attendance
//
//  Copyright © 2019 Giovanni Giacomo. All rights reserved.
//

import Foundation
import SwiftUI

struct Class: Codable, Hashable, Identifiable {
    var id: String
    var date: String
    var token: String
}
