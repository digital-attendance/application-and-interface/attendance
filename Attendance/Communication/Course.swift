//
//  Course.swift
//  Attendance
//
//  Copyright © 2019 Giovanni Giacomo. All rights reserved.
//

import Foundation
import SwiftUI

struct Course: Codable, Hashable, Identifiable {
    var id: String
    var name: String
    var description: String
}
