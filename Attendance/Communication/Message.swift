//
//  Message.swift
//  Attendance
//
//  Copyright © 2019 Giovanni Giacomo. All rights reserved.
//

import Foundation

// A basic structure for all messages coming to and from the server.
// Uses a header string that defines the type of message that follows,
// this allows for an initial parsing of the message to find the type.
struct Message: Codable {
    let header: String
}

// A message that defines how the login messages are structured.
// The server will receive the message in this format, process it
// appropriately and return an answer.
struct LoginMessage: Codable {
    let header: String = "login"
    let registration: String
    let password: String
}

// A definition for the structure of server answers to login messages.
// After processing the login message, the server will return a message
// in this form, with information relevant to the authentication.
struct LoginAnswerMessage: Codable {
    let header: String = "login-answer"
    let authenticated: Bool
    let id: String
    let type: String
}

//
//
//
struct RegisterMessage: Codable {
    let header: String = "register"
    let registration: String
    let course: String
    let name: String
    let password: String
}

//
//
//
struct RegisterAnswerMessage: Codable {
    let header: String = "register-answer"
    let authenticated: Bool
    let error: String
}

//
//
//
struct ListCoursesMessage: Codable {
    let header: String = "listCourses"
}

//
//
//
struct ListCoursesByUserMessage: Codable {
    let header: String = "listCoursesByUser"
    let registration: String
    let type: String
}

//
//
//
struct ListCoursesAnswerMessage: Codable {
    let header: String = "list-courses-answer"
    let courses: [Course]?
    let type: String
}

//
//
//
struct AddCourseMessage: Codable {
    let header: String = "addCourse"
    let professor: String
    let name: String
    let description: String
}

//
//
//
struct AddCourseAnswerMessage: Codable {
    let header: String = "add-course-answer"
    let authenticated: Bool
    let error: String
}

//
//
//
struct AddStudentToCourseMessage: Codable {
    let header: String = "addStudentToCourse"
    let course: String
    let student: String
}

//
//
//
struct AddStudentToCourseAnswerMessage: Codable {
    let header: String = "add-student-to-course-answer"
    let success: Bool
}

//
//
//
struct ListClassesByCourseMessage: Codable {
    let header: String = "listClassesByCourse"
    let course: String
}

//
//
//
struct ListClassesByUserMessage: Codable {
    let header: String = "listClassesByUser"
    let course: String
    let student: String
}

//
//
//
struct ListClassesAnswerMessage: Codable {
    let header: String = "list-classes-answer"
    let classes: [Class]?
    let type: String
}

//
//
//
struct RollcallMessage: Codable {
    let header: String = "rollcall"
    let course: String
    let professor: String
}

//
//
//
struct RollcallAnswerMessage: Codable {
    let header: String = "rollcall-answer"
    let authenticated: Bool
    let code: String
}

//
//
//
struct AttendanceMessage: Codable {
    let header: String = "attendance"
    let code: String
    let course: String
    let date: String
    let student: String
}

//
//
//
struct AttendanceAnswerMessage: Codable {
    let header: String = "attendance-answer"
    let authenticated: Bool
}

//
//
//
struct ListStudentsByAttendanceMessage: Codable {
    let header: String = "listStudentsByAttendance"
    let id: String
}

//
//
//
struct ListStudentsByClassMessage: Codable {
    let header: String = "listStudentsByClass"
    let id: String
}

//
//
//
struct ListStudentsAnswerMessage: Codable {
    let header: String = "list-students-answer"
    let students: [Student]?
    let type: String
}
