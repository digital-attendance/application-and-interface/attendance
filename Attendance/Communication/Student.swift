//
//  Student.swift
//  Attendance
//
//  Copyright © 2019 Giovanni Giacomo. All rights reserved.
//

import Foundation
import SwiftUI

struct Student: Codable, Hashable, Identifiable {
    var id: String
    var name: String
    var major: String
}
