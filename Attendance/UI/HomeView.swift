//
//  HomeView.swift
//  Attendance
//
//  Copyright © 2019 Giovanni Giacomo. All rights reserved.
//

import SwiftUI

struct HomeView: View {
    @EnvironmentObject private var user: User
    @State private var selectedView = 0
    
    @ViewBuilder
    var body: some View {
        if self.user.isLoggedIn == true {
            TabView {
                CourseView().environmentObject(user).tabItem {
                    Image(systemName: "book")
                    Text(verbatim: "Disciplinas")
                }
                
                MyCourseView().environmentObject(user).tabItem {
                    Image(systemName: "book.fill")
                    Text(verbatim: "Minhas Disciplinas")
                }
            }
        } else {
            LoginView().environmentObject(user)
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView().environmentObject(User())
    }
}
