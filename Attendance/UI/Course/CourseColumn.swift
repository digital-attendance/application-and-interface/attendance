//
//  CourseColumn.swift
//  Attendance
//
//  Copyright © 2019 Giovanni Giacomo. All rights reserved.
//

import SwiftUI

struct CourseColumn: View {
    @EnvironmentObject var user: User
    var courses: [Course]
    
    var body: some View {
        VStack(alignment: .center) {
            // A scrollable list with all the courses pertaining to a major.
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .leading) {
                    ForEach (courses) { course in
                        NavigationLink(destination: CourseDetail(course: course).environmentObject(self.user)) {
                            CourseItem(course: course)
                                .frame(width: 300)
                                .padding(.bottom)
                                .padding(.trailing, 30)
                        }
                    }
                }
            }
        }
    }
}

struct CourseColumn_Previews: PreviewProvider {
    static var previews: some View {
        CourseColumn(courses: courseData)
    }
}
