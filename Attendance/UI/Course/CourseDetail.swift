//
//  CourseDetail.swift
//  Attendance
//
//  Copyright © 2019 Giovanni Giacomo. All rights reserved.
//

import SwiftUI

struct CourseDetail: View {
    @EnvironmentObject var user: User
    var course: Course
    
    var body: some View {
        VStack(alignment: .leading) {
            List {
                //ScrollView(.vertical, showsIndicators: false) {
                    // Course image at the very top of the details pane.
                    ZStack(alignment: .bottom) {
                        Image("ImageExample")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                        
                        Rectangle()
                            .foregroundColor(.black)
                            .frame(height: 80)
                            .opacity(0.25)
                            .blur(radius: 10)
                        HStack {
                            VStack(alignment: .leading, spacing: 8) {
                                Text(course.name)
                                    .foregroundColor(.white)
                                    .font(.largeTitle)
                            }
                            .padding(.leading)
                            .padding(.bottom)
                            Spacer()
                        }
                    }
                    .listRowInsets(EdgeInsets())
                    
                    VStack(alignment: .leading) {
                        // Description of the course laid out as a text.
                        Text(course.description)
                            .font(.body)
                            .lineLimit(nil)
                            .lineSpacing(12)
                    }
                    .padding(.bottom)
                    .padding(.top)
                
                // List of all course rollcalls.
                if self.user.myCourses.contains(self.course) {
                    if self.user.userType == "Aluno" {
                        Text("Presenças (\(String(format: "%.2f", self.user.attendance))%):")
                            .font(.title)
                            .padding(.bottom)
                    } else if self.user.userType == "Professor" {
                        Text("Presenças:")
                            .font(.title)
                            .padding(.bottom)
                    }
                    
                    ForEach(self.user.allClasses) { myClass in
                        if self.user.userType == "Aluno" {
                            if self.user.myClasses.contains(myClass) {
                                HStack {
                                    Text(myClass.date)
                                    Spacer()
                                    Text("P")
                                        .foregroundColor(Color.green)
                                }
                            } else {
                                NavigationLink(destination: TokenEnter(course: self.course, date: myClass.date).environmentObject(self.user)) {
                                    HStack {
                                        Text(myClass.date)
                                        Spacer()
                                        Text("A")
                                            .foregroundColor(Color.red)
                                    }
                                }
                            }
                        } else if self.user.userType == "Professor" {
                            NavigationLink(destination: AttendanceList(classId: myClass.id, courseId: self.course.id).environmentObject(self.user)) {
                                HStack {
                                    Text(myClass.date)
                                    Spacer()
                                    Text(myClass.token)
                                }
                            }
                        }
                     }
                }
            }
            
            if self.user.myCourses.contains(self.course) {
                // Button to add new classes for professors.
                if user.userType == "Professor" {
                    HStack {
                        Spacer()
                        Button(action: {
                            let msg = saveJSON(RollcallMessage(course: self.course.id, professor: self.user.userId))!
                            
                            // Attempt to send message to server if connection has been established.
                            if self.user.ws.readyState == .open {
                                self.user.ws.send(msg)
                                print("Sent message: \(msg)")
                            } else {
                                print("Failed to send message: \(msg)")
                            }
                        }) {
                            Text("Nova Chamada")
                        }
                        .frame(width: 200, height: 50)
                        .foregroundColor(.white)
                        .font(.headline)
                        .background(Color.blue)
                        .cornerRadius(5)
                        .padding(.bottom)
                        Spacer()
                    }
                }
            } else {
                // Button to join course for students.
                if user.userType == "Aluno" {
                    HStack {
                        Spacer()
                        Button(action: {
                            let msg = saveJSON(AddStudentToCourseMessage(course: self.course.id, student: self.user.userId))!
                            
                            // Attempt to send message to server if connection has been established.
                            if self.user.ws.readyState == .open {
                                self.user.joinedCourse = self.course.id
                                self.user.ws.send(msg)
                                print("Sent message: \(msg)")
                            } else {
                                print("Failed to send message: \(msg)")
                            }
                        }) {
                            Text("Ingressar")
                        }
                        .frame(width: 200, height: 50)
                        .foregroundColor(.white)
                        .font(.headline)
                        .background(Color.blue)
                        .cornerRadius(5)
                        .padding(.bottom)
                        Spacer()
                    }
                }
            }
        }
        .edgesIgnoringSafeArea(.top)
        .navigationBarHidden(true)
        .onAppear(perform: {
            if self.user.myCourses.contains(self.course) {
                let allMsg = saveJSON(ListClassesByCourseMessage(course: self.course.id))!
                
                // Attempt to send message to server if connection has been established.
                if self.user.ws.readyState == .open {
                    self.user.ws.send(allMsg)
                    print("Sent message: \(allMsg)")
                } else {
                    print("Failed to send message: \(allMsg)")
                }
                
                if self.user.userType == "Aluno" {
                    let myMsg = saveJSON(ListClassesByUserMessage(course: self.course.id, student: self.user.userId))!
                    
                    // Attempt to send message to server if connection has been established.
                    if self.user.ws.readyState == .open {
                        self.user.ws.send(myMsg)
                        print("Sent message: \(myMsg)")
                    } else {
                        print("Failed to send message: \(myMsg)")
                    }
                }
            }
        })
        .alert(isPresented: $user.hasAddedRollcall) {
            Alert(title: Text("Sucesso"), message: Text("A nova chamada foi adicionada com sucesso."), dismissButton: .default(Text("Ok"), action: {
                if self.user.myCourses.contains(self.course) {
                    let msg = saveJSON(ListClassesByCourseMessage(course: self.course.id))!
                    
                    // Attempt to send message to server if connection has been established.
                    if self.user.ws.readyState == .open {
                        self.user.ws.send(msg)
                        print("Sent message: \(msg)")
                    } else {
                        print("Failed to send message: \(msg)")
                    }
                }
            }))
        }
    }
}

struct CourseDetail_Previews: PreviewProvider {
    static var previews: some View {
        CourseDetail(course: courseData[1])
    }
}
