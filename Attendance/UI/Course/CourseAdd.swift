//
//  CourseAdd.swift
//  Attendance
//
//  Copyright © 2019 Giovanni Giacomo. All rights reserved.
//

import SwiftUI

struct CourseAdd: View {
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @EnvironmentObject var user: User
    @State private var name: String = ""
    @State private var description: String = ""
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Spacer()
                Text("Nova Disciplina")
                    .foregroundColor(.primary)
                    .font(.title)
                Spacer()
            }
            .padding(.bottom)
            
            VStack {
                TextField("Nome", text: $name)
                Rectangle()
                    .foregroundColor(.gray)
                    .frame(height: 2)
                    .opacity(0.5)
                    .padding(.bottom)
                
                TextField("Descrição", text: $description)
                Rectangle()
                    .foregroundColor(.gray)
                    .frame(height: 2)
                    .opacity(0.5)
                    .padding(.bottom)
            }
            .padding(.horizontal)
            
            HStack {
                Spacer()
                VStack {
                    Button(action: {
                        let msg = saveJSON(AddCourseMessage(professor: self.user.userId, name: self.name, description: self.description))!
                        
                        // Attempt to send message to server if connection has been established.
                        if self.user.ws.readyState == .open {
                            self.user.ws.send(msg)
                            print("Sent message: \(msg)")
                        } else {
                            print("Failed to send message: \(msg)")
                        }
                    }) {
                        Text("Criar")
                    }
                    .frame(width: 200, height: 50)
                    .foregroundColor(.white)
                    .font(.headline)
                    .background(Color.blue)
                    .cornerRadius(5)
                    .padding(.bottom)
                }
                Spacer()
            }
            .padding(.top, 10)
            
            Spacer()
        }
        .alert(isPresented: $user.showAddCourseAlert) {
            switch user.activeAddCourseAlert {
            case .failed:
                return Alert(title: Text("Erro"), message: Text(user.addCourseMessage), dismissButton: .default(Text("Ok")))
            case .success:
                return Alert(title: Text("Sucesso"), message: Text(user.addCourseMessage), dismissButton: .default(Text("Ok"), action: { self.mode.wrappedValue.dismiss() }))
            }
        }
    }
}

struct CourseAdd_Previews: PreviewProvider {
    static var previews: some View {
        CourseAdd().environmentObject(User())
    }
}
