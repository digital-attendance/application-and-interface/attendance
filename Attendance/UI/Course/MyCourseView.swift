//
//  MyCourseView.swift
//  Attendance
//
//  Created by Giovanni Giacomo on 10/13/19.
//  Copyright © 2019 Giovanni Giacomo. All rights reserved.
//

import SwiftUI

struct MyCourseView: View {
    @EnvironmentObject var user: User
    
    var body: some View {
        NavigationView {
            VStack(alignment: .leading) {
                CourseColumn(courses: self.user.myCourses).environmentObject(self.user)
                    .padding(.bottom)
                    .padding(.leading, 30)
                    .padding(.top)
                    .navigationBarTitle(Text("Minhas Disciplinas"))
                
                // Button to add new course for professors.
                if user.userType == "Professor" {
                    HStack {
                        Spacer()
                        Button(action: {}) {
                            NavigationLink(destination: CourseAdd().environmentObject(self.user)) {
                                Text("Criar Disciplina")
                            }
                        }
                        .frame(width: 200, height: 50)
                        .foregroundColor(.white)
                        .font(.headline)
                        .background(Color.blue)
                        .cornerRadius(5)
                        .padding(.bottom)
                        .padding(.top)
                        Spacer()
                    }
                }
            }
        }
        .onAppear(perform: {
            let msg = saveJSON(ListCoursesByUserMessage(registration: self.user.userId, type: self.user.userType))!
            
            // Attempt to send message to server if connection has been established.
            if self.user.ws.readyState == .open {
                self.user.ws.send(msg)
                print("Sent message: \(msg)")
            } else {
                print("Failed to send message: \(msg)")
            }
        })
    }
}

struct MyCourseView_Previews: PreviewProvider {
    static var previews: some View {
        MyCourseView().environmentObject(User())
    }
}
