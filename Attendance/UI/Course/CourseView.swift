//
//  CourseView.swift
//  Attendance
//
//  Copyright © 2019 Giovanni Giacomo. All rights reserved.
//

import SwiftUI

struct CourseView: View {
    @EnvironmentObject var user: User
    
    var body: some View {
        NavigationView {
            VStack(alignment: .leading) {
                CourseColumn(courses: self.user.allCourses).environmentObject(self.user)
                    .padding(.bottom)
                    .padding(.leading, 30)
                    .padding(.top)
                    .navigationBarTitle(Text("Disciplinas"))
            }
        }
        .onAppear(perform: {
            let allMsg = saveJSON(ListCoursesMessage())!
            let myMsg = saveJSON(ListCoursesByUserMessage(registration: self.user.userId, type: self.user.userType))!
            
            // Attempt to send message to server if connection has been established.
            if self.user.ws.readyState == .open {
                self.user.ws.send(allMsg)
                self.user.ws.send(myMsg)
                print("Sent message: \(allMsg)")
                print("Sent message: \(myMsg)")
            } else {
                print("Failed to send message: \(allMsg)")
                print("Failed to send message: \(myMsg)")
            }
        })
    }
}

struct CourseView_Previews: PreviewProvider {
    static var previews: some View {
        CourseView().environmentObject(User())
    }
}
