//
//  CourseItem.swift
//  Attendance
//
//  Copyright © 2019 Giovanni Giacomo. All rights reserved.
//

import SwiftUI

struct CourseItem: View {
    var course: Course
    
    var body: some View {
        VStack(alignment: .leading, spacing: 16.0) {
            Image("ImageExample")
                .resizable()
                .renderingMode(.original)
                .aspectRatio(contentMode: .fill)
                .frame(width: 300, height: 170)
                .cornerRadius(10)
                .shadow(radius: 10)
            
            VStack(alignment: .leading, spacing: 5.0) {
                Text(course.name)
                    .foregroundColor(.primary)
                    .font(.headline)
                
                Text(course.description)
                    .foregroundColor(.primary)
                    .font(.subheadline)
                    .multilineTextAlignment(.leading)
                    .lineLimit(2)
                        .frame(height: 40)
            }
        }
    }
}

struct CourseItem_Previews: PreviewProvider {
    static var previews: some View {
        CourseItem(course: courseData[0])
    }
}
