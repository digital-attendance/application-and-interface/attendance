//
//  LoginView.swift
//  Attendance
//
//  Copyright © 2019 Giovanni Giacomo. All rights reserved.
//

import SwiftUI

struct LoginView: View {
    @EnvironmentObject var user: User
    @State private var registration: String = ""
    @State private var password: String = ""
    
    var body: some View {
        NavigationView {
            VStack(alignment: .leading) {
                HStack {
                    Spacer()
                    Text("Entrar")
                        .foregroundColor(.primary)
                        .font(.title)
                    Spacer()
                }
                .padding(.bottom)
                
                VStack {
                    TextField("Matrícula", text: $registration).textContentType(.username)
                    Rectangle()
                        .foregroundColor(.gray)
                        .frame(height: 2)
                        .opacity(0.5)
                        .padding(.bottom)
                    
                    SecureField("Senha", text: $password).textContentType(.password)
                    Rectangle()
                        .foregroundColor(.gray)
                        .frame(height: 2)
                        .opacity(0.5)
                        .padding(.bottom)
                }
                .padding(.horizontal)
                
                HStack {
                    Spacer()
                    VStack {
                        Button(action: {
                            let msg = saveJSON(LoginMessage(registration: self.registration, password: self.password))!
                            
                            // Attempt to send message to server if connection has been established.
                            if self.user.ws.readyState == .open {
                                self.user.ws.send(msg)
                                print("Sent message: \(msg)")
                            } else {
                                print("Failed to send message: \(msg)")
                            }
                        }) {
                            Text("Entrar")
                        }
                        .frame(width: 200, height: 50)
                        .foregroundColor(.white)
                        .font(.headline)
                        .background(Color.blue)
                        .cornerRadius(5)
                        .padding(.bottom)
                        
                        Text("Não possui uma conta? ")
                        NavigationLink(destination: RegisterView().environmentObject(user)) {
                            Text("Registre-se")
                        }
                    }
                    Spacer()
                }
                .padding(.top, 10)
                
                Spacer()
            }
        }
        .alert(isPresented: $user.hasFailedToLogin) {
            Alert(title: Text("Erro"), message: Text("Usuário ou senha incorretos."), dismissButton: .default(Text("Ok")))
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView().environmentObject(User())
    }
}
