//
//  AttendanceList.swift
//  Attendance
//
//  Copyright © 2019 Giovanni Giacomo. All rights reserved.
//

import SwiftUI

struct AttendanceList: View {
    @EnvironmentObject var user: User
    var classId: String
    var courseId: String
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Spacer()
                Text("Lista de Chamada")
                    .foregroundColor(.primary)
                    .font(.title)
                Spacer()
            }
            .padding(.bottom)
            
            List {
                ForEach(self.user.allStudents) { myStudent in
                    if self.user.myStudents.contains(myStudent) {
                        HStack {
                            Text(myStudent.id)
                            Spacer()
                            Text(myStudent.name)
                            Spacer()
                            Text("P")
                                .foregroundColor(Color.green)
                        }
                    } else {
                        HStack {
                            Text(myStudent.id)
                            Spacer()
                            Text(myStudent.name)
                            Spacer()
                            Text("A")
                                .foregroundColor(Color.red)
                        }
                    }
                }
            }
            
            Spacer()
        }
        .onAppear(perform: {
            let allMsg = saveJSON(ListStudentsByClassMessage(id: self.courseId))!
            let myMsg = saveJSON(ListStudentsByAttendanceMessage(id: self.classId))!
            
            // Attempt to send message to server if connection has been established.
            if self.user.ws.readyState == .open {
                self.user.ws.send(allMsg)
                self.user.ws.send(myMsg)
                print("Sent message: \(allMsg)")
                print("Sent message: \(myMsg)")
            } else {
                print("Failed to send message: \(allMsg)")
                print("Failed to send message: \(myMsg)")
            }
        })
    }
}

struct AttendanceList_Previews: PreviewProvider {
    static var previews: some View {
        AttendanceList(classId: "", courseId: "")
    }
}
