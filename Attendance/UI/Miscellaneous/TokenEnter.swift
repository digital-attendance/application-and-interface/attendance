//
//  TokenEnter.swift
//  Attendance
//
//  Copyright © 2019 Giovanni Giacomo. All rights reserved.
//

import SwiftUI

struct TokenEnter: View {
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @EnvironmentObject var user: User
    @State private var token: String = ""
    var course: Course
    var date: String
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Spacer()
                Text("Registrar Presença")
                    .foregroundColor(.primary)
                    .font(.title)
                Spacer()
            }
            .padding(.bottom)
            
            VStack {
                TextField("Código", text: $token)
                Rectangle()
                    .foregroundColor(.gray)
                    .frame(height: 2)
                    .opacity(0.5)
                    .padding(.bottom)
            }
            .padding(.horizontal)
            
            HStack {
                Spacer()
                VStack {
                    Button(action: {
                        let msg = saveJSON(AttendanceMessage(code: self.token, course: self.course.id, date: self.date, student: self.user.userId))!
                        
                        // Attempt to send message to server if connection has been established.
                        if self.user.ws.readyState == .open {
                            self.user.ws.send(msg)
                            print("Sent message: \(msg)")
                        } else {
                            print("Failed to send message: \(msg)")
                        }
                    }) {
                        Text("Enviar")
                    }
                    .frame(width: 200, height: 50)
                    .foregroundColor(.white)
                    .font(.headline)
                    .background(Color.blue)
                    .cornerRadius(5)
                    .padding(.bottom)
                }
                Spacer()
            }
            .padding(.top, 10)
            
            Spacer()
        }
        .alert(isPresented: $user.showTokenAlert) {
            switch user.activeTokenAlert {
            case .failed:
                return Alert(title: Text("Erro"), message: Text("O código inserido está incorreto ou a aula já encerrou."), dismissButton: .default(Text("Ok")))
            case .success:
                return Alert(title: Text("Sucesso"), message: Text("O código inserido está correto."), dismissButton: .default(Text("Ok"), action: { self.mode.wrappedValue.dismiss() }))
            }
        }
    }
}

struct TokenEnter_Previews: PreviewProvider {
    static var previews: some View {
        TokenEnter(course: courseData[0], date: "")
    }
}
