//
//  RegisterView.swift
//  Attendance
//
//  Copyright © 2019 Giovanni Giacomo. All rights reserved.
//

import SwiftUI

struct RegisterView: View {
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @EnvironmentObject var user: User
    @State private var registration: String = ""
    @State private var name: String = ""
    @State private var password: String = ""
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Spacer()
                Text("Criar Conta")
                    .foregroundColor(.primary)
                    .font(.title)
                Spacer()
            }
            .padding(.bottom)
            
            VStack {
                TextField("Matrícula", text: $registration).textContentType(.username)
                Rectangle()
                    .foregroundColor(.gray)
                    .frame(height: 2)
                    .opacity(0.5)
                    .padding(.bottom)
                
                TextField("Nome", text: $name).textContentType(.name)
                Rectangle()
                    .foregroundColor(.gray)
                    .frame(height: 2)
                    .opacity(0.5)
                    .padding(.bottom)
                
                SecureField("Senha", text: $password).textContentType(.newPassword)
                Rectangle()
                    .foregroundColor(.gray)
                    .frame(height: 2)
                    .opacity(0.5)
                    .padding(.bottom)
            }
            .padding(.horizontal)
            
            HStack {
                Spacer()
                VStack {
                    Button(action: {
                        let msg = saveJSON(RegisterMessage(registration: self.registration, course: "", name: self.name, password: self.password))!
                        
                        // Attempt to send message to server if connection has been established.
                        if self.user.ws.readyState == .open {
                            self.user.ws.send(msg)
                            print("Sent message: \(msg)")
                        } else {
                            print("Failed to send message: \(msg)")
                        }
                    }) {
                        Text("Registrar")
                    }
                    .frame(width: 200, height: 50)
                    .foregroundColor(.white)
                    .font(.headline)
                    .background(Color.blue)
                    .cornerRadius(5)
                    .padding(.bottom)
                }
                Spacer()
            }
            .padding(.top, 10)
            
            Spacer()
        }
        .alert(isPresented: $user.showRegisterAlert) {
            switch user.activeRegisterAlert {
            case .failed:
                return Alert(title: Text("Erro"), message: Text(user.registerMessage), dismissButton: .default(Text("Ok")))
            case .success:
                return Alert(title: Text("Sucesso"), message: Text(user.registerMessage), dismissButton: .default(Text("Ok"), action: { self.mode.wrappedValue.dismiss() }))
            }
        }
    }
}

struct RegisterView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterView().environmentObject(User())
    }
}
