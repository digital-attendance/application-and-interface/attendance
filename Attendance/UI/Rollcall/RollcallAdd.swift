//
//  RollcallAdd.swift
//  Attendance
//
//  Created by Giovanni Giacomo on 11/3/19.
//  Copyright © 2019 Giovanni Giacomo. All rights reserved.
//

import SwiftUI

struct RollcallAdd: View {
    @EnvironmentObject var user: User
    @State private var name: String = ""
    @State private var description: String = ""
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Spacer()
                Text("Nova Chamada")
                    .foregroundColor(.primary)
                    .font(.title)
                Spacer()
            }
            .padding(.bottom)
            
            VStack {
                TextField("Nome", text: $name)
                Rectangle()
                    .foregroundColor(.gray)
                    .frame(height: 2)
                    .opacity(0.5)
                    .padding(.bottom)
                
                TextField("Descrição", text: $description).lineLimit(nil)
                Rectangle()
                    .foregroundColor(.gray)
                    .frame(height: 2)
                    .opacity(0.5)
                    .padding(.bottom)
            }
            .padding(.horizontal)
            
            HStack {
                Spacer()
                VStack {
                    Button(action: {
                        let msg = saveJSON(AddCourseMessage(professor: self.user.userId, name: self.name, description: self.description))!
                        
                        // Attempt to send message to server if connection has been established.
                        if self.user.ws.readyState == .open {
                            self.user.ws.send(msg)
                            print("Sent message: \(msg)")
                        } else {
                            print("Failed to send message: \(msg)")
                        }
                    }) {
                        Text("Criar")
                    }
                    .frame(width: 200, height: 50)
                    .foregroundColor(.white)
                    .font(.headline)
                    .background(Color.blue)
                    .cornerRadius(5)
                    .padding(.bottom)
                }
                Spacer()
            }
            .padding(.top, 10)
            
            Spacer()
        }
        .alert(isPresented: $user.hasFailedToAddCourse) {
            Alert(title: Text("Erro"), message: Text(user.addCourseMessage), dismissButton: .default(Text("Ok")))
        }
        .alert(isPresented: $user.hasSuccededToAddCourse) {
            Alert(title: Text("Sucesso"), message: Text("A nova disciplina foi adicionada com sucesso."), dismissButton: .default(Text("Ok")))
        }
    }
}

struct RollcallAdd_Previews: PreviewProvider {
    static var previews: some View {
        RollcallAdd()
    }
}
